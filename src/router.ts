import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CompetenceController from "./controllers/CompetenceController";
import CriteresController from "./controllers/CriteresController";
import LoginController from "./controllers/LoginController";
import RegisterController from "./controllers/RegisterController";


export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/register', (req, res) =>
    {
        RegisterController.register(req, res);
    });

    app.post('/register', (req, res) =>{
        RegisterController.registerUser(req, res);
    })

    app.get('/competence/:id',(req, res) => {
        CompetenceController.competence(req, res);
    });

    app.post('/critere', (req, res) =>{
        CriteresController.critere(req, res);
    });

    app.get('/login', (req, res) =>{
        LoginController.login(req, res);
    })

    app.post('/login', (req, res) =>{
        LoginController.loginreq(req, res);
    })
}
