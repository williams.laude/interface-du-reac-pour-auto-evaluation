import { Request, Response } from "express-serve-static-core";
import session from 'express-session'

export default class LoginController {
    static login(req: Request, res: Response): void {
        res.render('pages/login')
    }

    static loginreq(req: Request, res: Response): void {
        const db = req.app.locals.db
        let email = req.body.email
        let password = req.body.password
        const user = db.prepare('SELECT * FROM USER WHERE email = ?').get(email)
        console.log(req.body)
       
            if(password === user.password) {
                req.session.username = user.username
                res.redirect('/')
            }else
            {
                res.render('pages/login',{
                    message: 'Incorrect email or password',
                })
            }

    }
}