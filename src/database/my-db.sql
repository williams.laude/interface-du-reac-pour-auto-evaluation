-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Mohamed
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-07 16:17
-- Created:       2021-12-06 15:04
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "COMPETENCE"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title_competence" VARCHAR(250) NOT NULL,
  "content_competence" LONGTEXT NOT NULL
);
CREATE TABLE "USER"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "email" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL,
  "created_at" DATETIME NOT NULL,
  "deleted_at" DATETIME NOT NULL,
  CONSTRAINT "email_UNIQUE"
    UNIQUE("email")
);
CREATE TABLE "CRITTER"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content_critter" LONGTEXT NOT NULL,
  "competence_id" INTEGER NOT NULL,
  CONSTRAINT "fk_CRITTER_COMPETENCE1"
    FOREIGN KEY("competence_id")
    REFERENCES "COMPETENCE"("id")
);
CREATE INDEX "CRITTER.fk_CRITTER_COMPETENCE1_idx" ON "CRITTER" ("competence_id");
CREATE TABLE "USER_has_CRITTER"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "user_id" INTEGER NOT NULL,
  "critter_id" INTEGER NOT NULL,
  CONSTRAINT "fk_USER_has_CRITTER_USER"
    FOREIGN KEY("user_id")
    REFERENCES "USER"("id"),
  CONSTRAINT "fk_USER_has_CRITTER_CRITTER1"
    FOREIGN KEY("critter_id")
    REFERENCES "CRITTER"("id")
);
CREATE INDEX "USER_has_CRITTER.fk_USER_has_CRITTER_CRITTER1_idx" ON "USER_has_CRITTER" ("critter_id");
CREATE INDEX "USER_has_CRITTER.fk_USER_has_CRITTER_USER_idx" ON "USER_has_CRITTER" ("user_id");
COMMIT;
